Déroulement d'une séance
########################

Soin thérapie de l'Âme :

C’est un soin complet qui permet de travailler à la fois sur le fond de la problématique (faille émotionnelle, douleur psychologique, source du mal être), que sur la forme avec le soin énergétique. Comme expliqué dans mon “qui suis-je”, je travaille essentiellement en guidance, c’est à dire que je m’adapte et adapte le soin en permanence selon les ressentis présents. Il est donc complexe de s’avancer sur le déroulement exact d’une séance comme ce dernier varie d’un soin à l’autre.

La séance dure généralement 1h20, mais selon les cas cela peut varier entre 50 min pour les enfants à 1h40 pour certains adultes. 

Au moment de la prise de rendez-vous : selon votre problématique et les autorisations de l’Univers, nous planifions ou non un rendez-vous en cabinet ou à distance. Si je perçois que je ne pourrai pas vous aider, il est possible que je vous oriente alors vers un thérapeute qui vous correspondra davantage. 

.. image:: https://celinedemoisson.com/wp-content/uploads/2020/08/t%C3%A9l%C3%A9chargement-1.jpeg

Au moment de la séance : Nous prenons premièrement le temps d’échanger plus profondément sur ce qui vous bloque et si vous avez une intention à déposer par rapport à la séance. Ensuite, le reste du soin se passe allongé (si vous êtes chez vous, il faudra prévoir un moment tranquille, avec une possibilité de me parler en restant allongé (kit mains libres, haut parleurs, ou, dans l’idéal, Skype ou visio)). La première partie de la séance est d’abord centrée sur la recherche de failles émotionnelles conscientes ou inconscientes et sur la mise en lumière et compréhension de ces dernières. Pour cela, je peux utiliser la kinésiologie, mon pendule, ou mon intuition. La deuxième partie viendra “corriger” ces blessures et “reprogrammer” l’inconscient vis à vis de ces dernières pour s’en libérer. Enfin, j’effectue un soin énergétique complet, qui permettra entres autres de nettoyer l’empreinte énergétique de la faille traitée et de la remplacer par de la lumière.  

Et encore

d'autres trucs.

Je conseille généralement de ne rien prévoir d’important après la séance et pour le reste de la journée.


