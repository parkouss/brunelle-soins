#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Brunelle Hutter'
SITENAME = 'Soins énergétiques'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = "../themes/simple"

PLUGINS = [
    "pelican_youtube",
]
